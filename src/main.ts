import { GameInfo } from './client/game-info.ts';
import { Engine } from './client/engine.ts';
import { Instance } from './client/instance.ts';

console.log(`======================================== TurboCanoe ========================================`);
console.log(`Unlock your WoWs FPS, enjoy the highest framerate your PC can deliver!`);
console.log(``);
console.log(`DISCLAIMER:`);
console.log(`This software comes as-is without warranty.`);
console.log(`This software is NOT sponsored by, endorsed by, or affiliated with Wargaming.net Limited`);
console.log(``);
console.log(`Author:    Aroop "FinlayDaG33k" Roelofs`);
console.log(`Website:   https://www.finlaydag33k.nl`);
console.log(`Donations: https://www.finlaydag33k.nl/donate`);
console.log(`============================================================================================`);

// Get our instance list
console.log(`Obtaining install directory...`);
const instance = new Instance();
await instance.loadInstance();
const dir = instance.getInstance;
console.log(`Detected installation at "${dir}"`)

// Load our game info
console.log("Loading game info...");
const gameInfo = await new GameInfo().load(dir);
const buildNumber = await gameInfo.getBuild;
console.log(`Detected WoWs client version "${gameInfo.getVersion}"!`);

// Unlock our FPS
console.log(`Unlocking FPS limiters...`);
const engine = new Engine();
await engine.loadConfig(`${dir}/bin/${buildNumber}`);
await engine.unlockFramerate();
engine.updateConfig(`${dir}/bin/${buildNumber}`);
console.log(`Framerates unlocked, enjoy your speedboost!`);

// Wait for the user to press a key
console.log(``);
console.log(`Press any key to exit TurboCanoe...`);
Deno.setRaw(Deno.stdin.rid, true);
await Deno.stdin.read(new Uint8Array(1));
Deno.exit(0);