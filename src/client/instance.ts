import { exists } from "https://deno.land/std/fs/mod.ts";

export class Instance {
  private instance: string = '';
  public get getInstance(): string { return this.instance; }

  public async loadInstance() {
    // Check if our expected config file exists
    if(!await exists(`C:\\ProgramData\\Wargaming.net\\GameCenter\\preferences.xml`))
      throw new Error(`Could not get a list of instances from "C:\\ProgramData\\Wargaming.net\\GameCenter\\preferences.xml"`);

    // Read our config file
    const config = await Deno.readTextFile(`C:\\ProgramData\\Wargaming.net\\GameCenter\\preferences.xml`);

    // Get our instances by regex
    const regex = /<WOWS>(.*)<\/WOWS>/;
    const match = regex.exec(config);
    if(!match) throw new Error(`No instance was found!`);
    if(match.length <= 1) throw new Error(`No instance was found!`);

    this.instance = match[1];
    return;
  }
}