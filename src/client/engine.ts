import { exists } from "https://deno.land/std/fs/mod.ts";

export class Engine {
  private config: string = '';
  private modded: boolean = false;

  public async loadConfig(dir: string): Promise<void> {
    // Try Check for a modded client
    // Load it's config if it exists
    if(await exists(`${dir}/res_mods`)) {
      this.modded = true;
      this.config = await Deno.readTextFile(`${dir}/res_mods/engine_config.xml`);
      return;
    }
    
    // Try check for an unmodded client
    // Load it's config if it exists
    if(await exists(`${dir}/res`)) {
      this.modded = false;
      this.config = await Deno.readTextFile(`${dir}/res/engine_config.xml`);
      return;
    }

    // Something did a fucksy
    throw new Error(`Could not find any client!`);
  }

  public async unlockFramerate() {
    this.config = this.config.replace(`<maxFrameRate>75</maxFrameRate>`, `<maxFrameRate>999</maxFrameRate>`);
  }

  public async updateConfig(dir: string) {
    const sub = this.modded ? 'res_mods' : 'res';
    await Deno.writeTextFile(`${dir}/${sub}/engine_config.xml`, this.config);
  }
}