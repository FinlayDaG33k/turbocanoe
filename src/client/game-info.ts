import { exists } from "https://deno.land/std/fs/mod.ts";

export class GameInfo {
  protected version: string = '';
  public get getVersion(): string { return this.version }

  public get getBuild() {
    const tokens = this.version.split('.');
    if(tokens.length <= 4) throw new Error('Could not obtain build number!');
    return tokens[4];
  }

  public async load(dir: string) {
    // Check if the directory exists
    const existing = await exists(`${dir}/game_info.xml`);
    if(!existing) throw new Error(`File "game_info" could not be found in "${dir}"...`);

    // Parse and read our XML
    const xml = await Deno.readTextFile(`${dir}/game_info.xml`);
    this.version = await this.parseVersion(xml);

    return this;
  }

  private parseVersion(xml: string): string {
    // Build our regex to obtain the client line
    let reg = /<version(?:.+)name="client"(?:.?)\/>/gm;

    // Execute our regex
    const matches = reg.exec(xml);
    if(!matches) throw new Error(`Could not find client version!`);
    if(matches.length < 1) throw new Error(`Could not find client version!`);
    const clientDef = matches[0];

    // Build our regex to obtain the version info
    reg = /installed="([0-9.]+)"/gm;

    // Execute our regex
    const version = reg.exec(clientDef);
    if(!version) throw new Error(`Could not find client version!`);
    if(version.length < 1) throw new Error(`Could not find client version!`);

    // Return our found version
    return version[1];
  }
}