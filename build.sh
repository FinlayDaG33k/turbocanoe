cp deno/deno.exe deno/deno-mod.exe
rcedit deno/deno-mod.exe --set-file-version 1.0.0.0
rcedit deno/deno-mod.exe --set-product-version 1.0.0
rcedit deno/deno-mod.exe --set-icon ./icon.ico
rcedit deno/deno-mod.exe --set-version-string "CompanyName" "Aroop \"FinlayDaG33k\" Roelofs"
rcedit deno/deno-mod.exe --set-version-string "FileDescription" "Unlock your World of Warships FPS"
rcedit deno/deno-mod.exe --set-version-string "LegalCopyright" "Aroop \"FinlayDaG33k\" Roelofs"
rcedit deno/deno-mod.exe --set-version-string "ProductName" "TurboCanoe"

rm -rf build
mkdir build
deno/deno-mod compile --unstable src/main.ts --output build/turbocanoe
